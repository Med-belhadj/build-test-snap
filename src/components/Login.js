import { signInWithGoogle } from "../services/firebase";
import { Button, Container, Row, Col } from "react-bootstrap";
import logo from "../images/logo.png";

import "../App.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGoogle } from "@fortawesome/free-brands-svg-icons";

const Login = () => {
  return (
    <div className="center">
      <div className="login">
        <Container>
          <Row>
            <Col></Col>
            <Row>
              <h2>Access to the movies room and enjoy your time !</h2>
            </Row>
            <Col></Col>
          </Row>
          <Row>
            <Col></Col>
            <Col>
              <img className="imageLogo" src={logo} alt="Logo" />
            </Col>
            <Col></Col>
          </Row>
          <Row>
            <Col></Col>
            <Col>
              <Button
                className="btnLogin"
                onClick={signInWithGoogle}
                exact
                variant="outline-success"
              >
                <FontAwesomeIcon className="googleLogo" icon={faGoogle} />
                Sign in with Google
              </Button>
            </Col>
            <Col></Col>
          </Row>
        </Container>
      </div>
    </div>
  );
};

export default Login;
